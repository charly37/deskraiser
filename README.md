# DESK RAISER

For Manoj !


## Versions

V1 : Version 3 of the proto
https://www.shapeways.com/model/upload-and-buy/4900486

V2 : Version 4 of the proto
Add a layer to kept the axe straight
Increase space between gear and holder
https://www.shapeways.com/model/upload-and-buy/5111609

V3 :
https://www.shapeways.com/model/upload-and-buy/5176633
After tests the gears are not powerfull enough and the motor too
Add more gears and change motor

V4 :
completly new design
https://www.shapeways.com/model/upload-and-buy/5275777
