# Documentation

## Hardware

![alt text](./Gear1.jpg "Logo Title Text 1")

[LINK](https://www.amazon.com/gp/product/B00KHUWC2W/ref=oh_aui_detailpage_o04_s01?ie=UTF8&psc=1) 

Type : Single
Teeth : 46
Diameter : 24 mm
High : 2 mm

![alt text](./Gear2.jpg "Logo Title Text 1")

Type : Double
Teeth : 38
Diameter : 20 mm
High : 1.5 mm
Teeth : 8
Diameter : 4.75 mm
High : 4.5 mm

![alt text](./Gear3.jpg "Logo Title Text 1")

Type : Double
Teeth : 34
Diameter : 18 mm
High : 1.5 mm
Teeth : 10
Diameter : 6 mm
High : 4 mm

Type : Single
Teeth : 18
Diameter : 10 mm
High : 4.75 mm

![alt text](./Screw1.jpg "Logo Title Text 1")

Screw:

[LINK](https://www.amazon.com/gp/product/B00GMYBQFG/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1) 

Lenght : 8 mm
High : 6 mm
1 teeth

Motor:

![alt text](./Motor.jpg "Logo Title Text 1")

[LINK](https://www.amazon.com/gp/product/B00AUCGG6U/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1) 

5000RPM
DC 6V

Axes:

![alt text](./Axe.jpg "Logo Title Text 1")

[LINK](https://www.amazon.com/gp/product/B00FHMA3D0/ref=oh_aui_detailpage_o07_s02?ie=UTF8&psc=1) 

diameter : 2 mm

## Speed

![alt text](./Gears.jpg "Logo Title Text 1")

5000 RPM Motor -> 147 RPM (/34) -> 38 RPM (/38*10) -> 6.5 RPM (/46*8)
