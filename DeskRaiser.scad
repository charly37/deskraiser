use <Libs.scad>;

aLargeur = 30;
aLongeur = 143;
aHauteur = 25;

aEpaisseurCoque = 4;
//for info only 
aDistanceEntreAxes = 14.6;  
aDistanceEntreAxesX = 13.1; 
aDistanceEntreAxesY = 2.75;  
aOffset = 28;

aSimuleRoue = false;

$fa=0.5;
$fs=0.5;


module FullShape(iLongeur,iLarguer,iHauteur)
{
    cylinder(h=iHauteur, d=iLarguer, center=false);
    translate(v = [0, -iLarguer/2,0])
    {  
        cube([iLongeur,iLarguer,iHauteur],center=false);
    }
    translate(v = [iLongeur, 0,0])
    {
        cylinder(h=iHauteur, d=iLarguer, center=false);
    }
}

module ExternalShape(iEpaisseur)
{
    difference()
    {
        FullShape(aLongeur,aLargeur,aHauteur);
        translate(v = [iEpaisseur/2, 0,-1])
        {
            FullShape(aLongeur-iEpaisseur,aLargeur-iEpaisseur,aHauteur+10);
        }
    }
    cylinder(h=iEpaisseur/2, d=aLargeur, center=false);
    translate(v = [aLongeur, 0,0])
    {
        cylinder(h=iEpaisseur/2, d=aLargeur, center=false);
    }
}

module Cover(iEpaisseur)
{
    difference()
    {
        intersection()
        { 
            union()
            {
                intersection()
                {
                    FullShape(aLongeur,aLargeur,aHauteur);
                    translate(v = [iEpaisseur/2, 0,+2])
                    {
                        FullShape(aLongeur-iEpaisseur,aLargeur-iEpaisseur,aHauteur+10);
                    }
                    
                }
                intersection()
                {
                    FullShape(aLongeur,aLargeur,aHauteur);
                    translate(v = [-20, -20,aHauteur-iEpaisseur])
                    cube([aLongeur+50,aLargeur+50,iEpaisseur]);
                }
            }
            translate(v = [0, 0,aHauteur-(1.5*iEpaisseur)])
            FullShape(aLongeur,aLargeur,2*iEpaisseur);
        }
        //Hole for DPDT switch
        translate(v = [2, -11.0,0])
        {
            cube([26.5,22.5,50]);
        }
        //Hole for power cable
        //translate(v = [40, 7,10])
        //{
            //cylinder(h=20, d=5, center=false);
        //}
        
        //Enleve matiere pour moteur
        translate(v = [34, -12,14])
        {
            cube([105,24,10]);
        }
    }
}

module ExternalShapeWithCover(iEpaisseur)
{
    ExternalShape(iEpaisseur);
    rotate(a=[180,0,0]) 
    {
        translate(v = [0, aLargeur+3,-aHauteur])
        {
            Cover(2);
        }
    }  
}

//support roue avec trou plus large
module supportRoue(aHauteur)
{
    difference()
    {
        cylinder(h=aHauteur, d=7, center=false);
        translate(v = [0, 0,-1])
        cylinder(h=aHauteur+2, d=5, center=false);
    }
}

//support roue avec trou meme taille
module supportRoueMemeTaille(aHauteur)
{
    difference()
    {
        cylinder(h=aHauteur, d=6, center=false);
        translate(v = [0, 0,-1])
        cylinder(h=aHauteur+2, d=2.2, center=false);
    }
}

module HelicoidaleSupport()
{
    ExternalShape(aEpaisseurCoque);

    translate(v = [aOffset, 0,0])
    {
        //le support pour le premier porte roue
        {
            translate(v = [-7/2, -15,0])
            cube([7,30,2]);
        }
        //la roue d en bas
        supportRoue(5);
        //le support pour le second porte roue
        {
            translate(v = [-7/2, -15,16])
            cube([7,30,2]);
        }
        //le second porte roue sur le support
        {
            translate(v = [0,0,18])
            supportRoue(3);
        }
        

                
        //simule la roue     5.75 -1 pour engre
        if(aSimuleRoue)
        {
            translate(v = [0, 0,7])
            color("DarkBlue")
            cylinder(h=5, d=4.75, center=false);
        }//Fin simule roue
        


        
        {//simule la roue 
        color("Turquoise")
        {
            translate(v = [-15, -aLargeur/2,8])
            {
                cube([4,13,5]);
                translate(v = [3, 0,-1])
                {
                    cube([3,30,6]);
                    translate(v = [21, 0,0])
                        cube([3,30,6]);
                    translate(v = [20, 13,0])
                        cube([1,6,6]);
                    translate(v = [35, 0,0])
                        cube([3,30,6]);
                }
                

            }
        }
        }
        
        //support bas du moteur 
        {
            translate(v = [25, -aLargeur/2,0])
            cube([60,aLargeur,aEpaisseurCoque/2]);
        }
        
        color("SlateGray")
        {
            translate(v = [50, -aLargeur/2,12])
            {
                cube([2,11.5,5]);
                //la bite pour bloquer le moteur
                rotate(a=[0,90,0]) 
                {
                    translate(v = [-2.5, 6.25,0])
                    {
                    cylinder(h=4, d=2, center=false);

                    }
                }
                translate(v = [0, 19.75,0])
                {
                    cube([2,10,5]);
                    //la bite pour bloquer le moteur
                    rotate(a=[0,90,0]) 
                    {
                        translate(v = [-2.5, 4.0,0])
                        cylinder(h=4, d=2, center=false);
                    }
                }
            }
        }
    }
}

module Tube3()
{
    difference()
    {
        hex(7,40);
        translate(v = [0, 0,-1])
        cylinder(h=25, d=2, center=false);
    }
}

module support()
{
    difference()
    {
        HelicoidaleSupport();
        translate(v = [aOffset, 0,-1])
        cylinder(h=50, d=2.5, center=false);
        
       
        translate(v = [aOffset-12, -5.0,10])
        rotate(a=[-0,90,0]) 
        cylinder(h=40, d=2.2, center=false);
        
        translate(v = [aOffset+9, -0.0,10])
        rotate(a=[-0,90,0]) 
        cylinder(h=40, d=2.2, center=false);
        
    }   
    
    translate(v = [aOffset-12, -5.0,10])
        {
            rotate(a=[-0,90,0]) 
            {
                //simule axe long 2
                {
                    //cylinder(h=45, d=2, center=false);
                }//simule axe long 2
                
                //la on simule la roue sur l axe non moteur  10-1
                if (aSimuleRoue)
                {
                    translate(v = [00, 0,7])
                    color("Lime")  
                    cylinder(h=10, d=5, center=false);
                }//la on simule la roue sur l axe non moteur  10-1
                
                //la on simule la roue sur l axe non moteur  10-1
                if (aSimuleRoue)
                {
                    translate(v = [0, 0,25])
                    color("Orange")  
                    cylinder(h=5, d=5, center=false);
                }//la on simule la roue sur l axe non moteur  10-1
                
                //la on simule la roue sur l axe non moteur  10-1
                if(aSimuleRoue)
                {
                    translate(v = [0, 5,25])
                    color("Magenta")  
                    cylinder(h=5, d=5, center=false);
                }//la on simule la roue sur l axe non moteur  10-1
                
                
            }
        }
        
}

module top()
{
    difference()
    {
        HelicoidaleSupport();
        translate(v = [0, 0,-1])
        cylinder(h=50, d=2.2, center=false);
    }
}


module print0()
{
    translate(v = [15, 20,7/2])
    rotate(a=[90,180,90]) 
    Tube3();
    
    support();
    
    //pour avoir une piece unique a imprimer
    translate(v = [15, 15,0])
    cube([2,10,2]); 
}

module print1()
{
    intersection()
    {
        print0();
        translate(v = [-20, -50,0])
        cube([120,80,80]); 
    }
}



print1();
//Tube3();

